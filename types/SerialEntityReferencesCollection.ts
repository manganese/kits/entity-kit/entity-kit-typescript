type SerialEntityReferencesCollection = {
  entityIds: object
};

export default SerialEntityReferencesCollection;