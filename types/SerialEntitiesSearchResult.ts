// Types
import SerialEntitiesCollection from "./SerialEntitiesCollection";


type SerialEntitiesSearchResult<ParametersType> = {
  entitiesCollection: SerialEntitiesCollection,
  entityIds: number[],
  parameters: ParametersType,
  count: number
};

export default SerialEntitiesSearchResult;