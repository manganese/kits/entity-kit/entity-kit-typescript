type SerialEntitiesCollection = {
  entities: object
};

export default SerialEntitiesCollection;