// Types
import SerialEntitiesCollection from "./SerialEntitiesCollection";
import SerialEntityReferencesCollection from "./SerialEntityReferencesCollection";


type SerialEntitiesUpdate = {
  updatedEntities: SerialEntitiesCollection,
  removedEntities: SerialEntityReferencesCollection
};

export default SerialEntitiesUpdate;