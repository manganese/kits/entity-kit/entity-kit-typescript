export default interface EntitiesPageSearchParameters {
  pageSize: number,
  page: number
};