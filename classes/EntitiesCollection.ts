// Classes
import Serializable from "core-kit/classes/Serializable";

import EntityReferencesCollection from "./EntityReferencesCollection";

// Types
import SerialEntitiesCollection from "../types/SerialEntitiesCollection";

// Interfaces
import Entity from "../interfaces/Entity";


/**

A collection of entities referenced by entity type and ID.

*/
export default class EntitiesCollection extends Serializable {
  /**

  Create a new entities collection, and optionally copy the data from an existing instance.

  Example of creating an empty entities collection:

  ```
  const entities = new EntitiesCollection();
  ```

  Example of creating an entities collection which copies data from another:

  ```typescript
  const entities1 = new EntitiesCollection().putEntity("widgets", { id: 31 });
  const entities2 = new EntitiesCollection(entities1);
  entities2.getEntity("widgets", 31); // { id: 31 }
  ```

  @param entitiesCollection  An entities collection from which to copy data.

  */
  constructor(entitiesCollection?: EntitiesCollection) {
    super();

    const entities = this.entities = new Map<string, Map<number, Entity>>();

    if (entitiesCollection) {
      entitiesCollection.getEntityTypes().forEach(entityType => {
        const entitiesOfType = entitiesCollection.getEntitiesOfType(entityType);

        entities.set(entityType, new Map<number, Entity>(entitiesOfType));
      });
    }
  }

  // Private state

  /** @internal */
  private entities: Map<string /* entity type */, Map<number /* entity ID */, Entity>>

  /**

  Get the internal entities data structure used by the entities collection.

  Note that the reference is preserved so modifying the value returned by this method can also affect the entities collection.

  @internal

  @category Unsafe

  */
  public unsafeGetEntities(): Map<string, Map<number, Entity>> {
    return this.entities;
  }

  /**

  Overwrite the internal entities data structure used by the entities collection.

  Note that calling this method may change the values of references to this entities collection.

  @internal

  @category Unsafe

  @param entities  The new entities data to set.

  */
  public unsafeSetEntities(entities: Map<string, Map<number, Entity>>): void {
    this.entities = entities;
  }

  /**

  Upsert an entity of a given type in the entities collection.  The entities collection is modified.

  Example:

  ```typescript
  const entities = new EntitiesCollection();
  entities.unsafePutEntity("widgets", { id: 33 });
  entities.getEntity("widgets", 33); // { id: 33 }
  ```

  @category Unsafe

  @param entityType  The entity type of the entity to put.
  @param entity  The entity to put.

  */
  public unsafePutEntity(entityType: string, entity: Entity): void {
    const entitiesOfType = (() => {
      if (this.entities.has(entityType)) {
        return this.entities.get(entityType);
      } else {
        return new Map<number, Entity>();
      }
    })();

    entitiesOfType.set(entity.id, entity);

    this.entities.set(entityType, entitiesOfType);
  }

  /**

  Remove an entity of a given type by its ID from the entities collection.  The entities collection is modified.

  Example:

  ```typescript
  const entities = new EntitiesCollection().putEntity("widgets", { id: 38 });
  entities.getEntity("widgets", 38); // { id: 38 }
  entities.unsafeRemoveEntity("widgets", 38);
  entities.getEntity("widgets", 38); // null
  ```

  @category Unsafe

  @param entityType  The entity type of the entity to remove.
  @param id  The ID of the entity to remove.

  */
  public unsafeRemoveEntity(entityType: string, id: number): void {
    const entitiesOfType = (() => {
      if (this.entities.has(entityType)) {
        return this.entities.get(entityType);
      } else {
        return new Map<number, Entity>();
      }
    })();

    entitiesOfType.delete(id);

    if (entitiesOfType.size === 0) {
      this.entities.delete(entityType);

      return;
    }

    this.entities.set(entityType, entitiesOfType);
  }

  // Public interface

  // Access

  /**

  Get the types of entities contained in this entities collection.

  Example:

  ```typescript
  const entities =
    new EntitiesCollection()
    .putEntity("widgets", { id: 38 })
    .putEntity("thingies", { id: 7 });
  entities.getEntityTypes(); // Set<string> { "widgets", "thingies" }
  ```

  @category Access

  */
  public getEntityTypes(): Set<string> {
    return new Set<string>(this.entities.keys());
  }

  /**

  Get a single entity of a given entity type by its ID from this entities collection.

  @category Access

  @typeParam EntityType  The type of the entity which is returned.

  @param entityType  The entity type of the entity to get.
  @param id  The ID of the entity to get.

  */
  public getEntity<EntityType extends Entity>(entityType: string, id: number): EntityType {
    if (!this.entities.has(entityType)) return null;

    return (this.entities.get(entityType).get(id) || null) as EntityType;
  }

  /**

  Get multiple entities of a given entity type by their IDs from this entities collection.

  @category Access

  @typeParam EntityType  The type of the entity which is returned.

  @param entityType  The entity type of the entities to get.

  */
  public getEntitiesOfType<EntityType extends Entity>(entityType: string): Map<number, EntityType> {
    return (this.entities.get(entityType) || new Map<number, Entity>()) as Map<number, EntityType>;
  }

  // Functional Mutation

  /**

  Put a single entity of a given entity type into a copy of this entities collection.

  @category Functional Mutation

  @param entityType  The entity type of the entity to put.
  @param entity  The entity to put.

  */
  public putEntity(entityType: string, entity: Entity): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);
    entitiesCollection.unsafePutEntity(entityType, entity);

    return entitiesCollection;
  }

  /**

  Put multiple entities of a given entity type into a copy of this entities collection.

  @category Functional Mutation

  @param entityType  The entity type of the entities to put.
  @param entitiesOfType  The entities of the given type to put.

  */
  public putEntitiesOfType(entityType: string, entitiesOfType: Set<Entity>): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);

    entitiesOfType.forEach(entity => {
      entitiesCollection.unsafePutEntity(entityType, entity);
    });

    return entitiesCollection;
  }

  /**

  Put entities of multiple entity types into a copy of this entities collection.

  @category Functional Mutation

  @param entities  A mapping of entity types to sets of entities to put.

  */
  public putEntities(entities: Map<string, Set<Entity>>): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);

    entities.forEach((entitiesOfType, entityType) => {
      entitiesOfType.forEach(entity => {
        entitiesCollection.unsafePutEntity(entityType, entity);
      });
    });

    return entitiesCollection;
  }

  /**

  Remove a single entity of a given entity type by its ID from a copy of this entities collection.

  @category Functional Mutation

  @param entityType  The entity type of the entity to remove.
  @param id  The ID of the entity to remove.

  */
  public removeEntity(entityType: string, id: number): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);
    entitiesCollection.unsafeRemoveEntity(entityType, id);

    return entitiesCollection;
  }

  /**

  Remove multiple entities of a given entity type from a copy of this entities collection.

  @category Functional Mutation

  @param entityType  The entity type of the entities to remove.
  @param idsOfType  The entity IDs of the given type to remove.

  */
  public removeEntitiesOfType(entityType: string, idsOfType: Set<number>): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);

    idsOfType.forEach(id => {
      entitiesCollection.unsafeRemoveEntity(entityType, id);
    });

    return entitiesCollection;
  }

  /**

  Remove entities which are referenced by a given entity references collection from a copy of this entities collection.

  @category Functional Mutation

  @param entityReferencesCollection  An entity references collection of entities to remove.

  */
  public removeEntities(entityReferencesCollection: EntityReferencesCollection): EntitiesCollection {
    const entitiesCollection = new EntitiesCollection(this);

    entityReferencesCollection.getEntityTypes().forEach(entityType => {
      entityReferencesCollection.getEntityIdsOfType(entityType).forEach(id => {
        entitiesCollection.unsafeRemoveEntity(entityType, id);
      });
    });

    return entitiesCollection;
  }

  /**

  Merge another entities collection into a copy of this entities collection.  Note that if an entity of the same entity type and ID exists in both, the merged entities collection will use values from the given entities collection.

  @category Functional Mutation

  @param entitiesCollection  An entities collection to merge with.

  */
  public merge(entitiesCollection: EntitiesCollection): EntitiesCollection {
    const mergedEntitiesCollection = new EntitiesCollection(this);

    entitiesCollection.unsafeGetEntities().forEach((entitiesOfType, entityType) => {
      entitiesOfType.forEach((entity, _id) => {
        mergedEntitiesCollection.unsafePutEntity(entityType, entity);
      });
    });

    return mergedEntitiesCollection;
  }

  // Serialization & Deserialization

  /**

  Serialize this entities collection as an object.

  @category Serialization & Deserialization

  */
  serialize(): SerialEntitiesCollection {
    return {
      entities: Array.from(this.entities.entries()).reduce((serializedEntities, entityTypeEntry) => {
        const entityType = entityTypeEntry[0];
        const entitiesOfType = entityTypeEntry[1];

        return {
          ...serializedEntities,
          [entityType]: Array.from(entitiesOfType.entries()).reduce((serializedEntitiesOfType, entityEntry) => {
            const id = entityEntry[0];
            const entity = entityEntry[1];

            return {
              ...serializedEntitiesOfType,
              [id]: entity
            };
          }, {})
        }
      }, {})
    };
  }

  /**

  Deserialize an entities collection from an object.

  @category Serialization & Deserialization

  */
  static deserialize(data: SerialEntitiesCollection): EntitiesCollection {
    const entities = new Map<string, Map<number, Entity>>(Object.keys(data.entities).map(entityType => {
      const entitiesOfTypeData = data.entities[entityType];
      const entitiesOfType = new Map<number, Entity>(Object.keys(entitiesOfTypeData).map(id => {
        const entity = entitiesOfTypeData[id];

        return [ Number(id), entity ];
      }));

      return [ entityType, entitiesOfType ];
    }));

    const entitiesCollection = new EntitiesCollection();
    entitiesCollection.unsafeSetEntities(entities);

    return entitiesCollection;
  }
};