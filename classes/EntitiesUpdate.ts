// Classes
import Serializable from "core-kit/classes/Serializable";

import EntitiesCollection from "./EntitiesCollection";
import EntityReferencesCollection from "./EntityReferencesCollection";

// Types
import SerialEntitiesUpdate from "../types/SerialEntitiesUpdate";

// Interfaces
import Entity from "../interfaces/Entity";

export default class EntitiesUpdate extends Serializable {
  constructor(entitiesUpdate?: EntitiesUpdate) {
    super();

    if (entitiesUpdate) {
      this.updatedEntities = new EntitiesCollection(entitiesUpdate.getUpdatedEntities());
      this.removedEntities = new EntityReferencesCollection(entitiesUpdate.removedEntities);
    } else {
      this.updatedEntities = new EntitiesCollection();
      this.removedEntities = new EntityReferencesCollection();
    }
  }

  // Private state
  private updatedEntities: EntitiesCollection
  private removedEntities: EntityReferencesCollection

  public unsafePutUpdatedEntities(updatedEntities: EntitiesCollection): void {
    this.updatedEntities = updatedEntities;
  }

  public unsafePutRemovedEntities(removedEntities: EntityReferencesCollection): void {
    this.removedEntities = removedEntities;
  }

  // Public interface
  public getUpdatedEntities(): EntitiesCollection {
    return this.updatedEntities;
  }

  public getEntity<EntityType extends Entity>(entityType: string, id: number): EntityType  {
    return this.updatedEntities.getEntity<EntityType>(entityType, id);
  }

  public putEntity(entityType: string, entity: Entity): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate(this);
    entitiesUpdate.unsafePutUpdatedEntities(entitiesUpdate.getUpdatedEntities().putEntity(entityType, entity));

    return entitiesUpdate;
  }

  public putEntitiesOfType(entityType: string, entitiesOfType: Set<Entity>): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate(this);
    entitiesUpdate.unsafePutUpdatedEntities(entitiesUpdate.getUpdatedEntities().putEntitiesOfType(entityType, entitiesOfType));

    return entitiesUpdate;
  }

  public putEntities(entities: Map<string, Set<Entity>>): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate(this);
    entitiesUpdate.unsafePutUpdatedEntities(entitiesUpdate.getUpdatedEntities().putEntities(entities));

    return entitiesUpdate;
  }

  public getRemovedEntities(): EntityReferencesCollection {
    return this.removedEntities;
  }

  public putRemovedEntityId(entityType: string, id: number): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate(this);
    entitiesUpdate.unsafePutRemovedEntities(entitiesUpdate.getRemovedEntities().putEntityId(entityType, id));

    return entitiesUpdate;
  }

  public putRemovedEntityIds(entityType: string, ids: Set<number>): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate(this);
    entitiesUpdate.unsafePutRemovedEntities(entitiesUpdate.getRemovedEntities().putEntityIds(entityType, ids));

    return entitiesUpdate;
  }

  public merge(entitiesUpdate: EntitiesUpdate): EntitiesUpdate {
    const mergedEntitiesUpdate = new EntitiesUpdate(this);
    mergedEntitiesUpdate.unsafePutUpdatedEntities(mergedEntitiesUpdate.getUpdatedEntities().merge(entitiesUpdate.getUpdatedEntities()));
    mergedEntitiesUpdate.unsafePutRemovedEntities(mergedEntitiesUpdate.getRemovedEntities().merge(entitiesUpdate.getRemovedEntities()));

    return mergedEntitiesUpdate;
  }

  // Serialization & Deserialization

  /**

  Serialize this entities update as an object.

  @category Serialization & Deserialization

  */
  serialize(): SerialEntitiesUpdate {
    return {
      updatedEntities: this.updatedEntities.serialize(),
      removedEntities: this.removedEntities.serialize()
    };
  }

  /**

  Deserialize an entities update from an object.

  @category Serialization & Deserialization

  */
  static deserialize(data: SerialEntitiesUpdate): EntitiesUpdate {
    const entitiesUpdate = new EntitiesUpdate();

    if (data.updatedEntities) {
      entitiesUpdate.unsafePutUpdatedEntities(EntitiesCollection.deserialize(data.updatedEntities));
    }

    if (data.removedEntities) {
      entitiesUpdate.unsafePutRemovedEntities(EntityReferencesCollection.deserialize(data.removedEntities));
    }

    return entitiesUpdate;
  }
};