// Classes
import Serializable from "core-kit/classes/Serializable";

import EntitiesCollection from "./EntitiesCollection";
import EntityReferencesCollection from "./EntityReferencesCollection";

// Types
import SerialEntitiesSearchResult from "../types/SerialEntitiesSearchResult";

// Interfaces
import EntitiesSearchParameters from "../interfaces/EntitiesSearchParameters";


export default class EntitiesSearchResult<ParametersType extends EntitiesSearchParameters> extends Serializable {
  constructor(data?: {
    entitiesCollection: EntitiesCollection,
    entityIds: number[],
    parameters: ParametersType,
    count: number
  }) {
    super();

    if (data) {
      this.entitiesCollection = data.entitiesCollection;
      this.entityIds = data.entityIds;
      this.parameters = data.parameters;
      this.count = data.count;
    } else {
      this.entitiesCollection = new EntitiesCollection();
      this.entityIds = [];
      this.parameters = null;
      this.count = 0;
    }
  }

  // Private state
  private entitiesCollection: EntitiesCollection
  private entityIds: number[]
  private parameters: ParametersType
  private count: number

  public unsafePutEntitiesCollection(entitiesCollection: EntitiesCollection): void {
    this.entitiesCollection = entitiesCollection;
  }

  public unsafePutEntityIds(entityIds: number[]): void {
    this.entityIds = entityIds;
  }

  public unsafePutParameters(parameters: ParametersType): void {
    this.parameters = parameters;
  }

  public unsafePutCount(count: number): void {
    this.count = count;
  }

  // Public interface
  public getEntitiesCollection(): EntitiesCollection {
    return this.entitiesCollection;
  }

  public getEntityIds(): number[] {
    return this.entityIds;
  }

  public getParameters(): ParametersType {
    return this.parameters;
  }

  public getCount(): number {
    return this.count;
  }

  // Serialization & Deserialization

  /**

  Serialize this entities update as an object.

  @category Serialization & Deserialization

  */
  serialize(): SerialEntitiesSearchResult<ParametersType> {
    return {
      entitiesCollection: this.entitiesCollection.serialize(),
      entityIds: this.entityIds,
      parameters: this.parameters,
      count: this.count
    };
  }

  /**

  Deserialize an entities update from an object.

  @category Serialization & Deserialization

  */
  static deserialize<ParametersType extends EntitiesSearchParameters>(data: SerialEntitiesSearchResult<ParametersType>): EntitiesSearchResult<ParametersType> {
    const entitiesUpdate = new EntitiesSearchResult<ParametersType>();
    entitiesUpdate.unsafePutEntitiesCollection(EntitiesCollection.deserialize(data.entitiesCollection));
    entitiesUpdate.unsafePutEntityIds(data.entityIds);
    entitiesUpdate.unsafePutParameters(data.parameters);
    entitiesUpdate.unsafePutCount(data.count);

    return entitiesUpdate;
  }
};