// Classes
import Serializable from "core-kit/classes/Serializable";

// Types
import SerialEntityReferencesCollection from "../types/SerialEntityReferencesCollection";


export default class EntityReferencesCollection extends Serializable {
  constructor(entityReferencesCollection?: EntityReferencesCollection) {
    super();

    const entityIds = this.entityIds = new Map<string, Set<number>>();

    if (entityReferencesCollection) {
      entityReferencesCollection.unsafeGetEntityIds().forEach((entityIdsOfType, entityType) => {
        entityIds.set(entityType, new Set<number>(entityIdsOfType));
      });
    }
  }

  // Private state
  private entityIds: Map<string /* entity type */, Set<number /* entity ID */>>

  public unsafeGetEntityIds(): Map<string, Set<number>> {
    return this.entityIds;
  }

  public unsafePutEntityIds(entityIds: Map<string, Set<number>>): void {
    this.entityIds = entityIds;
  }

  public unsafePutEntityId(entityType: string, id: number): void {
    const entityIdsOfType = (() => {
      if (this.entityIds.has(entityType)) {
        return this.entityIds.get(entityType);
      } else {
        return new Set<number>();
      }
    })();

    entityIdsOfType.add(id);

    this.entityIds.set(entityType, entityIdsOfType);
  }

  // Public interface
  public getEntityTypes(): Set<string> {
    return new Set<string>(this.entityIds.keys());
  }

  public getEntityIdsOfType(entityType: string): Set<number> {
    return this.entityIds.get(entityType) || null;
  }

  public putEntityId(entityType: string, id: number): EntityReferencesCollection {
    const entityReferencesCollection = new EntityReferencesCollection(this);
    entityReferencesCollection.unsafePutEntityId(entityType, id);

    return entityReferencesCollection;
  }

  public putEntityIds(entityType: string, ids: Set<number>): EntityReferencesCollection {
    const entityReferencesCollection = new EntityReferencesCollection(this);

    ids.forEach(id => {
      entityReferencesCollection.unsafePutEntityId(entityType, id);
    });

    return entityReferencesCollection;
  }

  public merge(entityReferencesCollection: EntityReferencesCollection): EntityReferencesCollection {
    const mergedEntityReferencesCollection = new EntityReferencesCollection(this);

    entityReferencesCollection.unsafeGetEntityIds().forEach((entityIdsOfType, entityType) => {
      entityIdsOfType.forEach(id => {
        mergedEntityReferencesCollection.unsafePutEntityId(entityType, id);
      });
    });

    return mergedEntityReferencesCollection;
  }

  // Serialization & Deserialization

  /**

  Serialize this entity references collection as an object.

  @category Serialization & Deserialization

  */
  serialize(): SerialEntityReferencesCollection {
    return {
      entityIds: Array.from(this.entityIds.entries()).reduce((serializedEntityIds, entityTypeEntry) => {
        const entityType = entityTypeEntry[0];
        const entityIdsOfType = entityTypeEntry[1];

        return {
          ...serializedEntityIds,
          [entityType]: Array.from(entityIdsOfType.values())
        }
      }, {})
    };
  }

  /**

  Deserialize an entity references collection from an object.

  @category Serialization & Deserialization

  */
  static deserialize(data: SerialEntityReferencesCollection): EntityReferencesCollection {
    const entityIds = new Map<string, Set<number>>(Object.keys(data.entityIds).map(entityType => {
      const entityIdsOfTypeData = data.entityIds[entityType];
      const entityIdsOfType = new Set<number>(entityIdsOfTypeData);

      return [ entityType, entityIdsOfType ];
    }));

    const entitiesCollection = new EntityReferencesCollection();
    entitiesCollection.unsafePutEntityIds(entityIds);

    return entitiesCollection;
  }
}