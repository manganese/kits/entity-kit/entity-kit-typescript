describe("EntitiesCollection class", () => {
  let EntitiesCollection
    , widget1
    , widget2
    ;

  beforeEach(() => {
    widget1 = {
      id: 1,
      name: "sam"
    };

    widget2 = {
      id: 2,
      name: "cadell"
    };

    EntitiesCollection = require("./EntitiesCollection").default;
  });

  describe("constructor", () => {
    describe("when given an existing entities collection", () => {
      it("copies the entities from the existing entities collection without modifying it", () => {
        const originalEntitiesCollection =
          new EntitiesCollection()
          .putEntity("widgets", widget1);

        const entitiesCollection = new EntitiesCollection(originalEntitiesCollection);

        expect(entitiesCollection.getEntity("widgets", 1)).toEqual(widget1);
      });
    });

    describe("when given no argument", () => {
      it("creates an empty entities collection", () => {
        const entitiesCollection = new EntitiesCollection();

        expect(entitiesCollection.unsafeGetEntities()).toEqual(new Map());
      });
    });
  });

  describe("#unsafeGetEntities", () => {

  });

  describe("#unsafeSetEntities", () => {

  });

  describe("#getEntityTypes", () => {
    it("returns a set of the entity types", () => {
      const entitiesCollection =
        new EntitiesCollection()
        .putEntity("widgets", widget1);

      expect(entitiesCollection.getEntityTypes()).toEqual(new Set([ "widgets" ]));
    });
  });

  describe("#getEntitiesOfType", () => {
    describe("when there are entities of the given entity type", () => {
      it("returns a map of the entities by ID", () => {
        const entitiesCollection =
          new EntitiesCollection()
          .putEntity("widgets", widget1)
          .putEntity("widgets", widget2);

        expect(entitiesCollection.getEntitiesOfType("widgets")).toEqual(new Map([
          [ 1, widget1 ],
          [ 2, widget2 ]
        ]));
      });
    });

    describe("when there are no entities of the given entity type", () => {
      it("returns an empty map", () => {
        const entitiesCollection = new EntitiesCollection();

        expect(entitiesCollection.getEntitiesOfType("widgets")).toEqual(new Map());
      });
    });
  });

  describe("#getEntity", () => {
    describe("when there is an entity of the given entity type and ID", () => {
      it("returns the entity", () => {
        const entitiesCollection = new EntitiesCollection();
        entitiesCollection.unsafePutEntity("widgets", widget1);

        expect(entitiesCollection.getEntity("widgets", 1)).toEqual(widget1);
      });
    });

    describe("when there is no entity of the given entity type and ID", () => {
      it("returns null", () => {
        const entitiesCollection = new EntitiesCollection();

        expect(entitiesCollection.getEntity("widgets", 2)).toBeNull();
      });
    });
  });

  describe("#putEntity", () => {
    it("returns a new entities collection with the entity upserted", () => {
      const originalEntitiesCollection = new EntitiesCollection();
      const entitiesCollection = originalEntitiesCollection.putEntity("widgets", widget1);

      expect(entitiesCollection.getEntity("widgets", 1)).toEqual(widget1);
      expect(entitiesCollection).not.toEqual(originalEntitiesCollection);
      expect(originalEntitiesCollection.getEntity("widgets", 1)).toBeNull();
    });
  });
});