// Classes
import EntitiesCollection from "../classes/EntitiesCollection";

// Interfaces
import { EntitiesState } from "../reducers/entities";

export default (entities: EntitiesState, entityType: string, id: number) => {
  return EntitiesCollection.deserialize(entities.entitiesCollection).getEntity(entityType, id);
};