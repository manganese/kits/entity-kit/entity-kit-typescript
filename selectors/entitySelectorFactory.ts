// Reselect
import { createSelector } from "reselect";

// Selectors
import entitySelector from "./entitySelector";

// Interfaces
import { EntitiesState } from "../reducers/entities";


const defaultEntitiesSelector = (state: { entities: EntitiesState }) => state.entities;


export default (entityType: string, id: number, entitiesSelector: (state: object) => EntitiesState = defaultEntitiesSelector) => {
  return createSelector(entitiesSelector, entities => entitySelector(entities, entityType, id));
};