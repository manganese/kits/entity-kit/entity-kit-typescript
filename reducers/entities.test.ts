describe("Entities reducer", () => {
  let entitiesReducer
    , EntitiesCollection
    , EntitiesUpdate
    , EntityReferencesCollection
    , widget1
    , widget2
    , state
    ;

  beforeEach(() => {
    widget1 = {
      id: 1,
      name: "sam"
    };

    widget2 = {
      id: 2,
      name: "cadell"
    };

    jest.isolateModules(() => {
      EntitiesCollection = require("../classes/EntitiesCollection").default;
      EntitiesUpdate = require("../classes/EntitiesUpdate").default;
      EntityReferencesCollection = require("../classes/EntityReferencesCollection").default;

      const entitiesCollection =
        new EntitiesCollection()
        .putEntity("widgets", widget1);

      state = {
        entitiesCollection
      };

      entitiesReducer = require("./entities").default;
    });
  });

  describe("when passed an action with the 'entiites' key", () => {
    fit("returns the state with the entities from the entities collection upserted", () => {
      const entitiesCollection =
        new EntitiesCollection()
        .putEntity("widgets", widget2);

      const action = {
        type: 'ANY',
        entities: entitiesCollection
      };

      const nextState = entitiesReducer(state, action);

      expect(nextState.entitiesCollection.getEntitiesOfType("widgets").size).toEqual(2);
      expect(nextState.entitiesCollection.getEntity("widgets", 2)).toEqual(widget2);
    });
  });

  describe("when passed an action with the 'entiitesUpdate' key", () => {
    it("returns the state with the entities from the entities update's entities collection upserted", () => {

    });

    it("returns the state with the entities from the entities update's removed entities deleted", () => {

    });

    describe("when the same entity is updated and removed", () => {
      it("returns the state with the entity deleted", () => {

      });
    });
  });

  describe("when passed an action with the 'removedEntiites' key", () => {
    it("returns the state with the entities from the removed entities deleted", () => {

    });
  });
});