// Classes
import EntitiesCollection from "../classes/EntitiesCollection";
import EntityReferencesCollection from "../classes/EntityReferencesCollection";
import EntitiesUpdate from "../classes/EntitiesUpdate";

// Types
import SerialEntitiesCollection from "../types/SerialEntitiesCollection";
import SerialEntityReferencesCollection from "../types/SerialEntityReferencesCollection";

// Interfaces
import Entity from "../interfaces/Entity";


export interface EntitiesState {
  entitiesCollection: SerialEntitiesCollection
}

function updateEntities(state: EntitiesState, entitiesCollection: EntitiesCollection): EntitiesState {
  if (!entitiesCollection) return state;

  return {
    ...state,
    entitiesCollection: EntitiesCollection.deserialize(state.entitiesCollection).merge(entitiesCollection).serialize()
  };
  // return (Array.from(entities.entities.entries()) as Array<[ string, Map<number, Entity> ]>).reduce((nextState, entry) => {
  //   const entityType = entry[0];
  //   const updatedEntities = entry[1] || new Map<number, Entity>();

  //   nextState.entities.set(entityType, (Array.from(updatedEntities.entries()) as Array<[ number, Entity ]>).reduce((entities, entry) => {
  //     const entityId = entry[0];
  //     const entity = entry[1];

  //     entities.set(entityId, entity);

  //     return entities;
  //   }, new Map<number, Entity>(nextState.entities.get(entityType))));

  //   return nextState;
  // }, { ...state });
}

function removeEntities(state: EntitiesState, removedEntities: EntityReferencesCollection): EntitiesState {
  if (!removedEntities) return state;

  return {
    ...state,
    entitiesCollection: EntitiesCollection.deserialize(state.entitiesCollection).removeEntities(removedEntities).serialize()
  };
  // return (Array.from(removedEntities.entityIds.entries()) as Array<[ string, Set<number> ]>).reduce((nextState, entry) => {
  //   const entityType = entry[0];
  //   const removedEntityIds = entry[1];

  //   nextState.entities.set(entityType, (Array.from(removedEntityIds.values()) as Array<number>).reduce((entities, removedEntityId) => {
  //     entities.delete(removedEntityId);

  //     return entities;
  //   }, new Map<number, Entity>(nextState.entities.get(entityType))));

  //   return nextState;
  // }, { ...state });
}

/*
  This keeps a client's version of entities in the Redux state.  This is the
  only place in the state that the entities in their full, serialized forms may
  exist - everything else should use IDs to reference them.

  All dispatched actions are handled by this reducer to determine if an entity
  update is present.  There are several ways that actions may update entities:

  1.  Update specific entities:

      {
        ...,
        entities: {
          [entityType]: {
            [entityId]: { ... },
            ...
          },
          ...
        }
      }

  2.  Apply a comprehensive entities update:

      {
        ...,
        entitiesUpdate: {
          entities: { ... },
          removedEntities: { ... }
        }
      }

  3.  Remove entities by ID:

      {
        ...,
        removedEntities: {
          [entityType]: [
            entityId
          ],
          ...
        }
      }
*/
export default (state: EntitiesState = {
  entitiesCollection: new EntitiesCollection().serialize()
}, action) => {
  let nextState = state;

  const {
    entities,
    entitiesUpdate,
    removedEntities
  } = action;

  if (entities) {
    nextState = updateEntities(nextState, EntitiesCollection.deserialize(entities));
  }

  if (entitiesUpdate) {
    nextState = updateEntities(nextState, EntitiesUpdate.deserialize(entitiesUpdate).getUpdatedEntities());
    nextState = removeEntities(nextState, EntitiesUpdate.deserialize(entitiesUpdate).getRemovedEntities());
  }

  if (removedEntities) {
    nextState = removeEntities(nextState, EntityReferencesCollection.deserialize(removedEntities));
  }

  return nextState;
};